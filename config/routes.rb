Rails.application.routes.draw do

  resources :logs

  resources :admin_settings

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }

  devise_scope :user do
    match 'users/:id/show', to: 'users/registrations#show', via: :get, as: "user_show"
    match 'users/', to: 'users/registrations#index', via: :get, as: "users_index"
    match 'users/:id/admin_edit', to: 'users/registrations#admin_edit', via: :get, as: "user_admin_edit"
    match 'users/:id/admin_update', to: 'users/registrations#admin_update', via: :patch, as: "user_admin_update"
  end

  #get 'users/:id', to: 'users/registrations#show'

  #match '/users/:id', to: 'users/registrations#show', via: :get

  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
